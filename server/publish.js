Polls = new Meteor.Collection2("polls", {
	 schema: {
        starttime: {
            type: String,
            label: "Daily starting time",
            max: 100
        },
        endtime: {
            type: String,
            label: "Daily ending time",
            max: 100
        },
        startdate: {
            type: String,
            label: "Poll start date",
            max: 100
        },
        enddate: {
            type: String,
            label: "Poll end date",
            max: 100
        },
    }
});

Meteor.publish('polls', function () {
  return Polls.find()
});

Availability = new Meteor.Collection2("availabilities", {
	schema: {
        user_id: {
            type: String,
            label: "user_id",
            max: 100
        },
        available: {
            type: String,
            label: "available",
        },
    }
});

Candidates = new Meteor.Collection2("candidates", {
   schema: {
        name: {
            type: String,
            label: "Name",
            max: 100
        },
        poll_id: {
            type: String,
            label: "Current Poll ID",
            max: 200
        },
        avail: {
            type: [String],
            label: "Availability",
        }

    }
});

Members = new Meteor.Collection2("members", {
	 schema: {
        name: {
            type: String,
            label: "name",
            max: 100
        },
        poll_id: {
            type: String,
            label: "poll_id",
            max: 200
        },        
        avail: {
            type: [String],
            label: "Availability",
        }
    }
});

Meteor.publish('candidates', function () {
  return Candidates.find()
});

Meteor.publish('members', function () {
  return Members.find()
});