// Local Search Algorithms
function Pair(candidate, val) {
	this.candidate = candidate
	this.val = val
}

function Vars(candidates) {
	this.candidates = []
	this.time_map = {}
	this.add_var = function(candidate) {
		this.candidates.push(candidate)
		for (var i in candidate.avail) {
			var time = candidate.avail[i]
			if (time in this.time_map) {
				this.time_map[time].push(candidate)
			} else {
				this.time_map[time] = [candidate]
			}
		}
	};
	for (var i in candidates) {
		this.add_var(candidates[i])
	}
}

function arr_remove(array, value) {
	var idx = array.indexOf(value)
	if (idx > -1) {
		array.splice(idx, 1)
	}
}

Vars.prototype.remove_var = function(candidate) {
	// Liable to crash if candidate not in vars
	arr_remove(this.candidates, candidate)
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		arr_remove(this.time_map[time], candidate)
	}
}

function Value(member, time) {
	this.member = member
	this.time = time
}

function Values(vals) {
	var times = []
	for (var time in vals) {
		times.push(time)
	}
	this.vals = vals
	this.times = times
}

Values.prototype.add_value = function(value) {
	if (value.time in this.vals) {
		this.vals[value.time].push(value)
	} else {
		this.vals[value.time] = [value]
		this.times.push(value.time)
	}
};

Values.prototype.has_value = function(time) {
	return (this.vals[time] && this.vals[time].length > 0)
};

Values.prototype.pop_value = function(time) {
	return this.vals[time].pop()
};

function get_values(members) {
	var values = new Values({})
	for (var i in members) {
		var member = members[i]
		for (var j in member.avail) {
			var time = member.avail[j]
			values.add_value(new Value(member, time))
		}
	}
	return values
}

function is_impossible(candidate, vals) {
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		if (vals.vals[time] && vals.vals[time].length != 0) {
			return false
		}
	}
	return true
}

function remove_impossible(vars, vals) {
	var impossible_vars = []
	for (var i in vars.candidates) {
		var candidate = vars.candidates[i]
		if (is_impossible(candidate, vals)) {
			vars.remove_var(candidate)
			impossible_vars.push("No members available at same time as " + candidate.name)
		}
	}
	return impossible_vars
}

function State(pairs, vals) {
	this.pairs = pairs
	this.vals = vals
}

function random_elt(arr, not) {
	var idx;
	if (not == undefined) {
		// Pick any key
		idx = Math.floor( Math.random() * arr.length )
	} else {
		// Don't pick 'not'
		idx = Math.floor( Math.random() * (arr.length-1) )
		if (idx >= arr.indexOf(not)) {
			idx += 1
		}
	}
	return arr[idx]
}

function random_key(vals, not) {
	var keys = Object.keys(obj)
	return random_elt(keys, not)
}

function clone_state(state) {
	var pairs = state.pairs.slice(0)
	var vals = new Values({})
	for (var time in state.vals.vals) {
		var arr = state.vals.vals[time]
		for (var j in arr) {
			vals.add_value(arr[j])
		}
	}
	return new State(pairs, vals)
}

function num_unused(vals) {
	var rv = 0
	for (var i in vals.times) {
		var time = vals.times[i]
		if (vals.vals[time]) {
			rv += vals.vals[time].length
		}
	}
	return rv
}

/*
	Code for completely randomized algorithm
	create_random_pairing
	Creates a random pairing of candidates to members, by
	iterating through all candidates and assigning a random member
*/
function create_random_pairing(members, candidates) {
	var pairings = []
	for (i in candidates) {
		var member = members[Math.floor(Math.random()*members.length)];
		pairings[i] = [candidates[i], member, null]
	}
	return pairings;
}

/* Quick and dirty set implementation using Objects */
function Set() {}
Set.prototype.add = function(o) {
	/* Returns whether we actually had to add it */
	if (this[o]) {
		return false
	} else {
		this[o] = true
		return true
	}
};
Set.prototype.remove = function(o) { delete this[o]; };

// score of a pairing defined by the number of candidates who have 
// a legitimate pairing (candidate availability matches member availability)
function get_score(pairings) {
	var score = 0
	var set = new Set()
	for (i in pairings) {
		var candidate = pairings[i][0]
		var member = pairings[i][1]
		for (j in candidate.avail) {
			if ($.inArray(candidate.avail[j], member.avail) > -1  && set.add(JSON.stringify(new Pair(member, candidate.avail[j])))) {
				pairings[i][2] = candidate.avail[j]
				score = score + 1;
				continue;
			}
		}
	}
	return score;
}
/*
	Generate random pairings creates pairings of (candidates, members) for 500 iterations.
	It scores each pairing based on the number of legitimate interviews, and returns
	the best overall pairing.
*/
generate_random_pairings = function generate_random_pairings(members, candidates) {
	var start = moment()
	var loops = 500
	var pairings = create_random_pairing(members, candidates)
	var score = get_score(pairings)
	for (var k = 0; k < loops; k++) {
		var neighbor = create_random_pairing(members, candidates)
		if (get_score(neighbor) > get_score(pairings)) {
			pairings = neighbor
		}
	}
	var matches = []
	var non_matches = []
	for (i in pairings) {
		var candidate = pairings[i][0]
		var member = pairings[i][1]
		var time = pairings[i][2]
		if (time) {
			matches.push(candidate.name + " interviewed by " + member.name + " at " + pairings[i][2])
		}
		else {
			non_matches.push("No interview found for " + candidate.name)
		}
	}
	var runtime = moment().diff(start)
	// Return value is an array of matches and an array of non_matches
	return {"matches": matches, "non_matches": non_matches, "runtime": runtime};
}

function generate_successor(state) {
	var cloned = clone_state(state)
	var pair = random_elt(cloned.pairs)
	var unused = num_unused(cloned.vals)
	if (Math.random() < unused / (unused + cloned.pairs.length - 1)) {
		/* Swap in an unused value */
		var time;
		do {
			time = random_elt(cloned.vals.times, pair.val.time)
		} while (!cloned.vals.vals[time] || cloned.vals.vals[time].length == 0)
		var newVal = cloned.vals.pop_value(time)
		cloned.vals.add_value(pair.val)
		pair.val = newVal
	} else {
		/* Swap with a different value */
		var pair2 = random_elt(cloned.pairs, pair)
		var val1 = pair.val
		pair.val = pair2.val
		pair2.val = val1
	}
	return cloned
}

/* 
	find best neighbor function is used in hill climbing.
	If there are fewer than 500 neighbors, this function will deterministically
	generate all possible neighbors and return the best. If there are more than 500
	neighbors, it will randomly generate 500 and return the best.
*/
function find_best_neighbor(state) {
	/* Possible over-estimate of the number of possible neighbors */
	var num_neighbors = (state.pairs.length * (state.pairs.length - 1)) / 2 + state.pairs.length * state.vals.times.length
	var best_neighbor = null
	var best_score = -Infinity
	/* Relatively few neighbors; go through all of them */
	if (num_neighbors < 500) {
		/* All swaps between pairs */
		for (var i = 0, len = state.pairs.length; i < len; i++) {
			for (var j = i+1; j < len; j++) {
				var neighbor = clone_state(state)
				var pair1 = neighbor.pairs[i]
				var pair2 = neighbor.pairs[j]
				var val1 = pair1.val
				pair1.val = pair2.val
				pair2.val = val1
				var score = get_score_hc(neighbor)
				if (score > best_score) {
					best_neighbor = neighbor
					best_score = score
				}
			}
		}
		/* All swaps with unused values */
		for (var i = 0, leni = state.pairs.length, lenj = state.vals.times.length; i < leni; i++) {
			for (var j = 0; j < lenj; j++) {
				var time = state.vals.times[j]
				var curtime = state.pairs[i].val.time
				if (time != curtime && state.vals.vals[time] && state.vals.vals[time].length > 0) {
					var neighbor = clone_state(state)
					var pair = neighbor.pairs[i]
					var newVal = neighbor.vals.pop_value(time)
					neighbor.vals.add_value(pair.val)
					pair.val = newVal
					var score = get_score_hc(neighbor)
					if (score > best_score) {
						best_neighbor = neighbor
						best_score = score
					}
				}
			}
		}
	} else {
		/* Lots of neighbors ... randomly sample 500 */
		for (var i = 0; i < 500; i++) {
			var neighbor = generate_successor(state)
			var score = get_score_hc(neighbor)
			if (score > best_score) {
				best_neighbor = neighbor
				best_score = score
			}
		}
	}
	return best_neighbor
}

function generate_neighbors(state) {
	var neighbors = []
	for (i = 0; i < 500; i++) {
		neighbors.push(generate_successor(state))
	}
	return neighbors
}

function get_score_hc(state) {
	var pairings = state.pairs
	var score = 0
	for (i in pairings) {
		var pair = pairings[i]
		var candidate = pair.candidate
		var time = pair.val.time
		if ($.inArray(time, candidate.avail) > -1) {
			score = score + 1;
		}
	}
	return score
}

function create_start_state(candidates, values) {
	var pairings = []
	for (var i in candidates) {
		var candidate = candidates[i]
		var time;
		do {
			time = random_elt(values.times)
		} while (!values.vals[time] || values.vals[time].length == 0)
		var value = values.pop_value(time)
		pairings.push(new Pair(candidate, value))
	}
	return new State(pairings, values)
}

/* our implementation of the hill climbing algorithm */
generate_hill_climbing = function generate_hill_climbing(members, candidates) {
	var start = moment()
	var values = get_values(members)
	var state = create_start_state(candidates, values)
	var score = get_score_hc(state)
	var best_state = state
	var best_score = score
	var total_iters = 0
	while(total_iters < 100) {
		var neighbor = find_best_neighbor(state)
		var currscore = get_score_hc(neighbor)
		if (currscore > best_score) {
			best_score = currscore
			best_state = neighbor
		}
		if (currscore > score) {
			score = currscore
			state = neighbor
		} else {
			values = get_values(members)
			state = create_start_state(candidates, values)
			score = get_score_hc(state)
			if (score > best_score) {
				best_score = score
				best_state = state
			}
		}
		total_iters++
	}
	state = best_state
	score = best_score
	var pairings = state.pairs
	var matches = []
	var non_matches = []
	for (var i in pairings) {
		var pair = pairings[i]
		var candidate = pair.candidate
		var time = pair.val.time
		if ($.inArray(time, candidate.avail) > -1) {
			matches.push(candidate.name + " interviewed by " + pair.val.member.name + " at " + time)
		}
		else {
			non_matches.push("No interview found for " + candidate.name)
		}
	}
	var runtime = moment().diff(start)

	// Return vale is still up for interpretation.
	// Likely return value should be an array of tuples that represents
	// a pairing, where each tuple is (member, candidate, time).
	//
	// We can use handlebars in the template to parse the array.
	return {"matches": matches, "non_matches": non_matches, "runtime": runtime};
}

// temperature function for simulated annealing
function T(iter) {
	return Math.pow(10, 7) * Math.pow(0.8, Math.floor(iter / 300))
}

function P(enew, e, k) {
	if (enew > e) {
		return 1
	}
	else {
		return Math.exp((enew - e) / T(k))
	}
}

// our implementation of simulated annealing
simulated_annealing = function simulated_annealing (members, candidates) {
	var start = moment()
	var values = get_values(members)
	var s = create_start_state(candidates, values)
	var e = get_score_hc(s)
	var sbest = s
	var ebest = e
	var k = 0
	var kmax = 25000
	while (k < kmax && e <= ebest) {
		snew = generate_successor(s)
		enew = get_score_hc(snew)
		//console.log(P(enew, e, k))
		if (P(enew, e, k) > Math.random()) {
			s = snew
			e = enew
		}
		if (enew > ebest) {
			sbest = snew
			ebest = enew
		}
		k = k + 1
	}
	var state = sbest
	var pairings = state.pairs
	var matches = []
	var non_matches = []
	for (var i in pairings) {
		var pair = pairings[i]
		var candidate = pair.candidate
		var time = pair.val.time
		if ($.inArray(time, candidate.avail) > -1) {
			matches.push(candidate.name + " interviewed by " + pair.val.member.name + " at " + time)
		}
		else {
			non_matches.push("No interview found for " + candidate.name)
		}
	}
	var runtime = moment().diff(start)
	return {"matches": matches, "non_matches": non_matches, "runtime": runtime}
}

// Get the result of our randomly generated pairings.
Template.pollResult.result_random = function () {
	if (Session.get('random_pairings')) {
		return Session.get('random_pairings');
	}
	else {
		if (Session.get('currentpoll')) {
			var id = Session.get('currentpoll')._id
			var members = Members.find({poll_id: id}).fetch()
			var candidates = Candidates.find({poll_id: id}).fetch()
			return generate_random_pairings(members, candidates)
		}
		else {
			return null
		}
	}
}

Template.pollResult.events({
  'click #regenerate_random': function () {
  		var id = Session.get('currentpoll')._id
			var members = Members.find({poll_id: id}).fetch()
			var candidates = Candidates.find({poll_id: id}).fetch()
  		var pairings = generate_random_pairings(members, candidates);
			Session.set('random_pairings', pairings);
	}
})

// Get the result of our hill climbing generated pairings.
Template.pollResult.result_hc = function () {
		return Session.get('hill_climbing');
}

Template.pollResult.events({
  'click #regenerate_hill_climbing': function () {
  		var id = Session.get('currentpoll')._id
			var members = Members.find({poll_id: id}).fetch()
			var candidates = Candidates.find({poll_id: id}).fetch()
  		var pairings = generate_hill_climbing(members, candidates);
			Session.set('hill_climbing', pairings);
	}
})

// Get the result of our hill climbing generated pairings.
Template.pollResult.result_sa = function () {
		return Session.get('sim_anneal');
}

Template.pollResult.events({
  'click #regenerate_sim_anneal': function () {
  		var id = Session.get('currentpoll')._id
			var members = Members.find({poll_id: id}).fetch()
			var candidates = Candidates.find({poll_id: id}).fetch()
  		var pairings = simulated_annealing(members, candidates);
			Session.set('sim_anneal', pairings);
	}
})

