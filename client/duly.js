Polls = new Meteor.Collection2("polls", {
   schema: {
        starttime: {
            type: String,
            label: "starttime",
            max: 100
        },
        endtime: {
            type: String,
            label: "endtime",
            max: 100
        },
        startdate: {
            type: String,
            label: "startdate",
            max: 100
        },
        enddate: {
            type: String,
            label: "enddate",
            max: 100
        },
    }
});

Candidates = new Meteor.Collection2("candidates", {
   schema: {
        name: {
            type: String,
            label: "Name",
            max: 100
        },
        poll_id: {
            type: String,
            label: "Current Poll ID",
            max: 200
        },
        avail: {
            type: [String],
            label: "Availability",
        }

    }
});

Members = new Meteor.Collection2("members", {
   schema: {
        name: {
            type: String,
            label: "Name",
            max: 100
        },
        poll_id: {
            type: String,
            label: "Current Poll ID",
            max: 200
        },
        avail: {
            type: [String],
            label: "Availability",
        }
    }
});

Availability = new Meteor.Collection2("availabilities", {
  schema: {
        user_id: {
            type: String,
            label: "user_id",
            max: 100
        },
        available: {
            type: String,
            label: "available",
        },
    }
});

var pollsHandle = Meteor.subscribe('polls');
var membersHandle = Meteor.subscribe('members');
var candidatesHandle = Meteor.subscribe('candidates');

Template.timepicker.events({
  'click #go': function () {
    var fromDate = $('#dpd1').val();
    var untilDate = $('#dpd2').val();
    var fromTime = $('#from').val();
    var untilTime = $('#until').val();
    poll_id = Polls.insert({startdate: fromDate, enddate: untilDate, starttime: fromTime, endtime: untilTime}, 
      function(error, result) {
        if (error) {
          alert(Polls.namedContext("default").invalidKeys()[0].message);
        }
        else {
          Router.go('pollShow', {_id: poll_id});
        }
    })
  }
});

// prepare time and date picker when the template is rendered
Template.timepicker.rendered = function () {
  // code for timepicker
  $('.time-picker').timepicker({ 'forceRoundTime': true, 'scrollDefaultNow': true, 'step':60});
  $('#time').hide()
  var nowTemp = new Date();
  var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} var today = mm+'/'+dd+'/'+yyyy;
  $('#dpd1').val(today);
  var checkin = $('#dpd1').datepicker({
    onRender: function(date) {
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate() + 1);
      checkout.setValue(newDate);
    }
    checkin.hide();
    $('#dpd2')[0].focus();
  }).data('datepicker');
  var checkout = $('#dpd2').datepicker({
    onRender: function(date) {
      return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    $('#time').show()
    checkout.hide();
  }).data('datepicker');
};

Template.polls.polls = function () {
  return Polls.find({})
};

Template.form.hours = function () {
  if (Session.get('currentpoll')) {
    var starttime = moment(Session.get('currentpoll').starttime, "hh:mma");
    var endtime = moment(Session.get('currentpoll').endtime,"hh:mma");
    var hours = [starttime.format('h:mma')]
    var timediff = endtime.diff(starttime, 'hours')
    for (j = 1; j < timediff; j++) {
      newHour = starttime.clone().add('hours', j)
      newHour = newHour.format('h:mma')
      hours.push(newHour);
    }
    return hours
  }
}

Template.form.days = function () {
  if (Session.get('currentpoll')) {
    var startdate = moment(Session.get('currentpoll').startdate);
    var enddate = moment(Session.get('currentpoll').enddate);
    var datediff = enddate.diff(startdate, 'days') // 1
    var days = [startdate.format('ddd MMM DD YYYY')]
    for (i = 1; i < datediff+1; i++) {
      newDay = startdate.clone().add('days', i)
      newDay = newDay.format('ddd MMM DD YYYY');
      days.push(newDay)
    }
    return days
  }
}

Template.members.members = function () {
  if (Session.get('currentpoll')) {
    return Members.find({poll_id: Session.get('currentpoll')._id})
  }
};

Template.candidates.candidates = function () {
  if (Session.get('currentpoll')) {
    return Candidates.find({poll_id: Session.get('currentpoll')._id})
  }
};

Template.pollShow.events({
  'click #addavail': function () {
    var id = Session.get('currentpoll')._id;
    var name = $('#name').val();
    $('#name').val('');
    var role = $('input:radio[name=role]:checked').val();
    var availability = []
    $('input[type=checkbox]').each(function () {
        var thisVal = (this.checked ? $(this).val() : "");
        if (thisVal) {
          availability.push(thisVal);
        }
        $(this).attr('checked', false);
    });
    if (role == 'member') {
      member_id = Members.insert({name: name, poll_id: id, avail: availability}, 
        function(error, result) {
          if (error) {
            alert(Members.namedContext("default").invalidKeys()[0].message);
          }
          else {
            Router.go('pollShow', {_id: id});
          }
        })
    }
    else {
      candidate_id = Candidates.insert({name: name, poll_id: id, avail: availability}, 
        function(error, result) {
          if (error) {
            alert(Candidates.namedContext("default").invalidKeys()[0].message);
          }
          else {
            Router.go('pollShow', {_id: id});
          }
      })
    }

  }
});

// Meteor Iron Router
Router.configure({
  loadingTemplate: 'loading'
});

Router.map( function () {

  this.route('timepicker', {
    path: '/',
  });

  this.route('playground', {
    path: '/playground',
  });

  this.route('pollShow', {
    path: '/polls/:_id',
    notFoundTemplate: 'notFound',
    waitOn: function() {
      return Meteor.subscribe('polls');
    },
    before: function() {
        poll = Polls.findOne({_id: this.params._id});
        Session.set('currentpoll', poll);
    },
    data: function () {
      poll = Polls.findOne({_id: this.params._id});
      data = {
        _id: this.params._id,
        poll: poll,
      }
      return data;
    },
  });

  this.route('pollResult', {
    path: '/polls/:_id/result',
    notFoundTemplate: 'notFound',
    waitOn: function() {
      return Meteor.subscribe('polls');
    },
    before: function() {
        poll = Polls.findOne({_id: this.params._id});
        Session.set('currentpoll', poll);
        Session.set('random_pairings', null);
        Session.set('hill_climbing', null);
        Session.set('sim_anneal', null);
    },
    data: function () {
      poll = Polls.findOne({_id: this.params._id});
      data = {
        _id: this.params._id,
        poll: poll,
      }
      return data;
    },
  });

  this.route('about');

  this.route('notFound', {
    path: '*'
  });
});


// Meteor Autoform callbacks https://github.com/aldeed/meteor-autoform
Polls.callbacks({ 
  remove: function(error) {
    if (error) {
      alert("Uh oh! Something went wrong when trying to remove this poll.");
    }
    else {
      Router.go('timepicker');
    }
  }
})

Meteor.startup(function () {

});
