/* A Solution contains the current pairing + the deepest (best) imperfect solution found */
function Solution() {
	this.pairs = []
	this.deepest = -1
	this.best_imperfect = []
	this.abandoned = []
}

/* A list of candidates + a map of time->candidate list, for use in calculating degree */
function Vars(candidates) {
	this.candidates = []
	this.time_map = {}
	this.add_var =
		/* Add a candidate to a Vars object (defined here because of scoping issues) */
		function(candidate) {
			/* Add to list of candidates */
			this.candidates.push(candidate)
			/* Update time_map */
			for (var i in candidate.avail) {
				var time = candidate.avail[i]
				if (time in this.time_map) {
					this.time_map[time].push(candidate)
				} else {
					this.time_map[time] = [candidate]
				}
			}
		};
	for (var i in candidates) {
		this.add_var(candidates[i])
	}
}

/* helper to remove an object from an array */
function arr_remove(array, value) {
	var idx = array.indexOf(value)
	if (idx > -1) {
		array.splice(idx, 1)
	}
}

/* Remove a candidate from a Vars object */
Vars.prototype.remove_var = function(candidate) {
	arr_remove(this.candidates, candidate)
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		arr_remove(this.time_map[time], candidate)
	}
}

/* A tuple of a candidate and a value */
function Pair(candidate, val) {
	this.candidate = candidate
	this.val = val
}

/* A tuple of a member and a time */
function Value(member, time) {
	this.member = member
	this.time = time
}

/* Class for storing all values: a list of times + a map of time -> values with that time */
function Values(vals) {
	var times = []
	for (var time in vals) {
		times.push(time)
	}
	this.vals = vals
	this.times = times
}

/* Add a value to a Values object */
Values.prototype.add_value = function(value) {
	if (value.time in this.vals) {
		this.vals[value.time].push(value)
	} else {
		this.vals[value.time] = [value]
		this.times.push(value.time)
	}
};

/* Check whether a Values object has a value for a particular time */
Values.prototype.has_value = function(time) {
	return (this.vals[time] && this.vals[time].length > 0)
};

/* Pop a value with a particular time from a Values object;
 * should be used only if has_value returns true
 */
Values.prototype.pop_value = function(time) {
	return this.vals[time].pop()
};

/* Takes in a list of members and return a Values object
 * containing a Value for each valid member, time pair
 */
function get_values(members) {
	var values = new Values({})
	for (var i in members) {
		var member = members[i]
		for (var j in member.avail) {
			var time = member.avail[j]
			values.add_value(new Value(member, time))
		}
	}
	return values
}

/* Returns true if the candidate is obviously unsatisfiable:
 * the candidate's times do not intersect with any members
 */
function is_impossible(candidate, vals) {
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		if (vals.vals[time] && vals.vals[time].length > 0) {
			return false
		}
	}
	return true
}

/* Will remove any impossible candidates from the Vars object */
function remove_impossible(vars, vals) {
	var impossible_vars = []
	for (var i in vars.candidates) {
		var candidate = vars.candidates[i]
		if (is_impossible(candidate, vals)) {
			vars.remove_var(candidate)
			impossible_vars.push("No members available at same time as " + candidate.name)
		}
	}
	return impossible_vars
}

/* Calculates the number of remaining legal values for the candidate */
remaining_values = function remaining_values(candidate, vals) {
	var rv = 0
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		if (vals.vals[time]) {
			rv += vals.vals[time].length
		}
	}
	return rv
}

/* Quick and dirty set implementation using Objects */
function Set() {}
Set.prototype.add = function(o) {
	/* Returns whether we actually had to add it */
	if (o in this && this[o]) {
		return false
	} else {
		this[o] = true
		return true
	}
};
Set.prototype.remove = function(o) { delete this[o]; };

/* Calculates the degree of a candidate in the interference graph;
 * two candidates interfere if their availabilities intersect.
 */
function degree(candidate, vars) {
	/* Start at -1 to compensate for counting candidate */
	var deg = -1
	var set = new Set()
	for (var i in candidate.avail) {
		var time = candidate.avail[i]
		var others = vars.time_map[time]
		for (var j in others) {
			var other = others[j]
			if (set.add(other)) {
				deg += 1
			}
		}
	}
	return deg
}

/* Select next variable to assign a value to, using min remaining values heuristic, 
 * with highest degree as a tie-breaker */
function select_next_var(vars, vals) {
	var best = null
	var best_mrv = Infinity
	var best_deg = -Infinity
	for (var i in vars.candidates) {
		var candidate = vars.candidates[i]
		var mrv = remaining_values(candidate, vals)
		if (best == null || mrv <= best_mrv) {
			/* Only calculate degree if we have to */
			var deg = degree(candidate, vars)
			if (best == null || mrv < best_mrv || (mrv == best_mrv && deg > best_deg)) {
				best = candidate
				best_mrv = mrv
				best_deg = deg
			}
		}
	}
	return best
}

/* Returns the candidates times sorted according to how constraining they are */
function sorted_times(candidate, vars, vals) {
	/* Sort times based on number of candidates who want it */
	candidate.avail.sort(function(a, b) {
		return vars.time_map[a].length - vars.time_map[b].length
	});
	return candidate.avail
}

/* Performs a forward-checking backtracking search, using MRV, highest degree, 
 * and least-constraining value heuristics
 */
function rec_backtracking_search(partial_pairing, vars, vals, depth) {
	/* Save current pairings, if this is the deepest (best) we've gotten */
	if (depth > partial_pairing.deepest) {
		partial_pairing.deepest = depth
		partial_pairing.best_imperfect = partial_pairing.pairs.slice(0)
		partial_pairing.abandoned = vars.candidates.slice(0)
	}
	if (vars.candidates.length == 0) {
		/* Success! Solution returned via side-effects */
		return true
	}
	var candidate = select_next_var(vars, vals)
	var times = sorted_times(candidate, vars, vals)
	for (var i in times) {
		var time = times[i]
		/* Only times consistent with the candidate are seen here */
		if (vals.has_value(time)) {
			/* Apply value to parameters */
			var val = vals.pop_value(time)
			vars.remove_var(candidate)
			partial_pairing.pairs.push(new Pair(candidate, val))
			
			var res = rec_backtracking_search(partial_pairing, vars, vals, depth+1)
			if (res) {
				/* Success */
				return res
			}
			/* Undo changes to parameters */
			vals.add_value(val)
			vars.add_var(candidate)
			partial_pairing.pairs.pop()
		}
	}
	return false
}

/* Does some initial set-up before calling the backtracking search function */
backtracking_pairings = function backtracking_pairings(members, candidates) {
	var start = moment()

	/* Initialize parameters */
	var vars = new Vars(candidates)
	var vals = get_values(members)
	var solution = new Solution()
	
	/* Remove impossible values before calling search function */
	var non_matches = remove_impossible(vars, vals)
	var impossible = non_matches
	/* Might have to repeat because we're modifying the array while iterating through it */
	while (impossible.length > 0) {
		impossible = remove_impossible(vars, vals)
		non_matches = non_matches.concat(impossible)
	}
	var matches = []
	var success = rec_backtracking_search(solution, vars, vals, 0)
	
	var pairing;
	if (success) {
		/* Extract the perfect solution */
		pairing = solution.pairs
	} else {
		/* Extract the best imperfect solution and record candidates without interviews */
		pairing = solution.best_imperfect
		for (var i in solution.abandoned) {
			non_matches.push("No match found for " + solution.abandoned[i].name)
		}
	}
	/* Record candidates with interviews */
	for (var i in pairing) {
		var pair = pairing[i]
		matches.push(pair.candidate.name + " interviewed by " + pair.val.member.name + " at " + pair.val.time)
	}
	var runtime = moment().diff(start)
	// We can use handlebars in the template to parse the array.
	return {"matches": matches, "non_matches": non_matches, "runtime": runtime}
}


Template.pollResult.result_backtracking = function () {
	if (Session.get('backtracking_pairings')) {
		return Session.get('backtracking_pairings');
	}
	else {
		if (Session.get('currentpoll')) {
			var id = Session.get('currentpoll')._id
			var members = Members.find({poll_id: id}).fetch()
			var candidates = Candidates.find({poll_id: id}).fetch()
			return backtracking_pairings(members, candidates)
		}
		else {
			return null
		}
	}
};

Template.pollResult.events({
	  'click #regenerate': function () {
	  	 	var id = Session.get('currentpoll')._id
				var members = Members.find({poll_id: id}).fetch()
				var candidates = Candidates.find({poll_id: id}).fetch()
	  		var pairings = backtracking_pairings(members, candidates);
				Session.set('backtracking_pairings', pairings);
		}
	}) 